# Run Chrome Headless in a container
#
# What was once a container using the experimental build of headless_shell from
# tip, this container now runs and exposes stable Chrome headless via
# google-chome --headless.
#
# What's New
#
# 1. Pulls from Chrome Stable
# 2. You can now use the ever-awesome Jessie Frazelle seccomp profile for Chrome.
#     wget https://raw.githubusercontent.com/jfrazelle/dotfiles/master/etc/docker/seccomp/chrome.json -O ~/chrome.json
#
#
# To run (without seccomp):
# docker run -d -p 9222:9222 --cap-add=SYS_ADMIN justinribeiro/chrome-headless
#
# To run a better way (with seccomp):
# docker run -d -p 9222:9222 --security-opt seccomp=$HOME/chrome.json justinribeiro/chrome-headless
#
# Basic use: open Chrome, navigate to http://localhost:9222/
#

# Base docker image
# FROM debian:buster-slim as builder
# LABEL name="micro-news-env" \
#   maintainer="Aaron R Miller <amiller@watheia.org>" \
#   version="1.0" \
#   description="Build Environment for Micro News"

# # Install deps + add Chrome Stable + purge all the things
# RUN apt-get update && apt-get install -y \
#   apt-transport-https \
#   ca-certificates \
#   curl \
#   gnupg \
#   --no-install-recommends \
#   && curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
#   && echo "deb https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
#   && apt-get update && apt-get install -y \
#   google-chrome-stable \
#   fontconfig \
#   fonts-ipafont-gothic \
#   fonts-wqy-zenhei \
#   fonts-thai-tlwg \
#   fonts-kacst \
#   fonts-symbola \
#   fonts-noto \
#   fonts-freefont-ttf \
#   --no-install-recommends \
#   && apt-get purge --auto-remove -y curl gnupg \
#   && rm -rf /var/lib/apt/lists/*

# # Add an unprivliged user
# RUN groupadd -r wa && useradd -r -g wa -G audio,video wa \
#   && mkdir -p /home/wa && chown -R wa:wa /home/wa

# # Run wa non-privileged
# USER wa

# # Expose ports for debugging
# EXPOSE 9222


# Serve the BDD report as the frontend service
FROM node:alpine

LABEL name="micro-news" \
  maintainer="Aaron R Miller <amiller@watheia.org>" \
  version="0.1" \
  description="Micro Frontend News by Watheia Labs"

COPY app /app
COPY target/site/serenity app/public/screenplay

WORKDIR /app
RUN yarn install && yarn build

# Set auto-devops defaults
ENV HOST=0.0.0.0 PORT=5000
EXPOSE ${PORT}

CMD [ "yarn", "start" ]

