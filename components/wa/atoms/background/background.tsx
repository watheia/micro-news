import React from 'react';

export type BackgroundProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function Background({ text }: BackgroundProps) {
  return (
    <div>
      {text}
    </div>
  );
}
