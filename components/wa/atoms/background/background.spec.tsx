import React from 'react';
import { render } from '@testing-library/react';
import { BasicBackground } from './background.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicBackground />);
  const rendered = getByText('hello from Background');
  expect(rendered).toBeTruthy();
});
