import React from 'react';
import { DarkModeToggler } from './dark-mode-toggler';

export const BasicDarkModeToggler = () => (
  <DarkModeToggler text="hello from DarkModeToggler" />
);
