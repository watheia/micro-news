import React from 'react';

export type DarkModeTogglerProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function DarkModeToggler({ text }: DarkModeTogglerProps) {
  return (
    <div>
      {text}
    </div>
  );
}
