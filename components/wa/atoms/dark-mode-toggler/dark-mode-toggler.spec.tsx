import React from 'react';
import { render } from '@testing-library/react';
import { BasicDarkModeToggler } from './dark-mode-toggler.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicDarkModeToggler />);
  const rendered = getByText('hello from DarkModeToggler');
  expect(rendered).toBeTruthy();
});
