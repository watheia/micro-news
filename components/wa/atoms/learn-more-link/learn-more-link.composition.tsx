import React from 'react';
import { LearnMoreLink } from './learn-more-link';

export const BasicLearnMoreLink = () => (
  <LearnMoreLink text="hello from LearnMoreLink" />
);
