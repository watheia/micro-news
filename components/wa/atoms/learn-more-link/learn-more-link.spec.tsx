import React from 'react';
import { render } from '@testing-library/react';
import { BasicLearnMoreLink } from './learn-more-link.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicLearnMoreLink />);
  const rendered = getByText('hello from LearnMoreLink');
  expect(rendered).toBeTruthy();
});
