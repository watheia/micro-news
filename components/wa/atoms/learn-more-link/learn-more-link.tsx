import React from 'react';

export type LearnMoreLinkProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function LearnMoreLink({ text }: LearnMoreLinkProps) {
  return (
    <div>
      {text}
    </div>
  );
}
