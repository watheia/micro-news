import React from 'react';

export type SwiperNumberProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function SwiperNumber({ text }: SwiperNumberProps) {
  return (
    <div>
      {text}
    </div>
  );
}
