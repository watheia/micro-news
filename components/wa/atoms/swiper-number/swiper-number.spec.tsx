import React from 'react';
import { render } from '@testing-library/react';
import { BasicSwiperNumber } from './swiper-number.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicSwiperNumber />);
  const rendered = getByText('hello from SwiperNumber');
  expect(rendered).toBeTruthy();
});
