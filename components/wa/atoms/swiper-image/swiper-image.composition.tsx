import React from 'react';
import { SwiperImage } from './swiper-image';

export const BasicSwiperImage = () => (
  <SwiperImage text="hello from SwiperImage" />
);
