import React from 'react';

export type SwiperImageProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function SwiperImage({ text }: SwiperImageProps) {
  return (
    <div>
      {text}
    </div>
  );
}
