import React from 'react';
import { render } from '@testing-library/react';
import { BasicSwiperImage } from './swiper-image.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicSwiperImage />);
  const rendered = getByText('hello from SwiperImage');
  expect(rendered).toBeTruthy();
});
