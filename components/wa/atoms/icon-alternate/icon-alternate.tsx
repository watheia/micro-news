import React from 'react';

export type IconAlternateProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function IconAlternate({ text }: IconAlternateProps) {
  return (
    <div>
      {text}
    </div>
  );
}
