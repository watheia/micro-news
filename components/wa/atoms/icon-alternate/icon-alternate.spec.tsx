import React from 'react';
import { render } from '@testing-library/react';
import { BasicIconAlternate } from './icon-alternate.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicIconAlternate />);
  const rendered = getByText('hello from IconAlternate');
  expect(rendered).toBeTruthy();
});
