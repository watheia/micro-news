import React from 'react';
import { IconAlternate } from './icon-alternate';

export const BasicIconAlternate = () => (
  <IconAlternate text="hello from IconAlternate" />
);
