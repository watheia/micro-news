import React from 'react';

export type ScrollTopProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function ScrollTop({ text }: ScrollTopProps) {
  return (
    <div>
      {text}
    </div>
  );
}
