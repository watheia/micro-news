import React from 'react';
import { ScrollTop } from './scroll-top';

export const BasicScrollTop = () => (
  <ScrollTop text="hello from ScrollTop" />
);
