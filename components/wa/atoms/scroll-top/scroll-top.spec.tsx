import React from 'react';
import { render } from '@testing-library/react';
import { BasicScrollTop } from './scroll-top.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicScrollTop />);
  const rendered = getByText('hello from ScrollTop');
  expect(rendered).toBeTruthy();
});
