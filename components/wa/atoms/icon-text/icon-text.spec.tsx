import React from 'react';
import { render } from '@testing-library/react';
import { BasicIconText } from './icon-text.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicIconText />);
  const rendered = getByText('hello from IconText');
  expect(rendered).toBeTruthy();
});
