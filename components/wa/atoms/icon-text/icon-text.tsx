import React from 'react';

export type IconTextProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function IconText({ text }: IconTextProps) {
  return (
    <div>
      {text}
    </div>
  );
}
