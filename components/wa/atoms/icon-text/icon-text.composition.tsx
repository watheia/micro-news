import React from 'react';
import { IconText } from './icon-text';

export const BasicIconText = () => (
  <IconText text="hello from IconText" />
);
