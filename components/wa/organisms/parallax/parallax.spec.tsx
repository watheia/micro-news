import React from 'react';
import { render } from '@testing-library/react';
import { BasicParallax } from './parallax.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicParallax />);
  const rendered = getByText('hello from Parallax');
  expect(rendered).toBeTruthy();
});
