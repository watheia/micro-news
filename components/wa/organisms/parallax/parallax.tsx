import React from 'react';

export type ParallaxProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function Parallax({ text }: ParallaxProps) {
  return (
    <div>
      {text}
    </div>
  );
}
