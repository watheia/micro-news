import React from 'react';
import { Parallax } from './parallax';

export const BasicParallax = () => (
  <Parallax text="hello from Parallax" />
);
