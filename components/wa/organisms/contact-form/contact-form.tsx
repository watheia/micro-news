import React from 'react';

export type ContactFormProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function ContactForm({ text }: ContactFormProps) {
  return (
    <div>
      {text}
    </div>
  );
}
