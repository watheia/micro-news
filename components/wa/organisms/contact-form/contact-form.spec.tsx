import React from 'react';
import { render } from '@testing-library/react';
import { BasicContactForm } from './contact-form.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicContactForm />);
  const rendered = getByText('hello from ContactForm');
  expect(rendered).toBeTruthy();
});
