import React from 'react';
import { render } from '@testing-library/react';
import { BasicPlain } from './plain.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicPlain />);
  const rendered = getByText('hello from Plain');
  expect(rendered).toBeTruthy();
});
