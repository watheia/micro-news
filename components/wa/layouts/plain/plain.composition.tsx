import React from 'react';
import { Plain } from './plain';

export const BasicPlain = () => (
  <Plain text="hello from Plain" />
);
