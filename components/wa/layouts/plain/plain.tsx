import React from 'react';

export type PlainProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function Plain({ text }: PlainProps) {
  return (
    <div>
      {text}
    </div>
  );
}
