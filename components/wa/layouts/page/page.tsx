import React from 'react';

export type PageProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function Page({ text }: PageProps) {
  return (
    <div>
      {text}
    </div>
  );
}
