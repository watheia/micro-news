import React from 'react';
import { render } from '@testing-library/react';
import { BasicDocument } from './document.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicDocument />);
  const rendered = getByText('hello from Document');
  expect(rendered).toBeTruthy();
});
