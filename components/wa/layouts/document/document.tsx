import React from 'react';

export type DocumentProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function Document({ text }: DocumentProps) {
  return (
    <div>
      {text}
    </div>
  );
}
