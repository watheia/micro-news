import { renderHook, act } from '@testing-library/react-hooks';
import { useLayout } from './use-layout';

it('should increment counter', () => {
  const { result } = renderHook(() => useLayout())
  act(() => {
    result.current.increment()
  })
  expect(result.current.count).toBe(1)
})
