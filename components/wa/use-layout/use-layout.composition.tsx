import React from 'react';
import { useLayout } from './use-layout';

export const BasicuseLayout = () => {
  const { count, increment } = useLayout();

  return (
    <>
      <h1>The count is {count}</h1>
      <button onClick={increment}>increment</button>
    </>
  );
};
