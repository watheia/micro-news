import React from 'react';

export type OverlappedImagesProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function OverlappedImages({ text }: OverlappedImagesProps) {
  return (
    <div>
      {text}
    </div>
  );
}
