import React from 'react';
import { render } from '@testing-library/react';
import { BasicOverlappedImages } from './overlapped-images.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicOverlappedImages />);
  const rendered = getByText('hello from OverlappedImages');
  expect(rendered).toBeTruthy();
});
