import React from 'react';
import { render } from '@testing-library/react';
import { BasicHeroShaped } from './hero-shaped.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicHeroShaped />);
  const rendered = getByText('hello from HeroShaped');
  expect(rendered).toBeTruthy();
});
