import React from 'react';

export type HeroShapedProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function HeroShaped({ text }: HeroShapedProps) {
  return (
    <div>
      {text}
    </div>
  );
}
