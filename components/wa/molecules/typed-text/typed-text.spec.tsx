import React from 'react';
import { render } from '@testing-library/react';
import { BasicTypedText } from './typed-text.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicTypedText />);
  const rendered = getByText('hello from TypedText');
  expect(rendered).toBeTruthy();
});
