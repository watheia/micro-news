import React from 'react';

export type TypedTextProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function TypedText({ text }: TypedTextProps) {
  return (
    <div>
      {text}
    </div>
  );
}
