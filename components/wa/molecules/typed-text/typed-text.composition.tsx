import React from 'react';
import { TypedText } from './typed-text';

export const BasicTypedText = () => (
  <TypedText text="hello from TypedText" />
);
