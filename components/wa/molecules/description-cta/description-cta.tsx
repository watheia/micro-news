import React from 'react';

export type DescriptionCtaProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function DescriptionCta({ text }: DescriptionCtaProps) {
  return (
    <div>
      {text}
    </div>
  );
}
