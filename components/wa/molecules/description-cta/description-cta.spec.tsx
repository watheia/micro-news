import React from 'react';
import { render } from '@testing-library/react';
import { BasicDescriptionCta } from './description-cta.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicDescriptionCta />);
  const rendered = getByText('hello from DescriptionCta');
  expect(rendered).toBeTruthy();
});
