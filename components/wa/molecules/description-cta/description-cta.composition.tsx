import React from 'react';
import { DescriptionCta } from './description-cta';

export const BasicDescriptionCta = () => (
  <DescriptionCta text="hello from DescriptionCta" />
);
