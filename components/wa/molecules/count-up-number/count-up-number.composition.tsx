import React from 'react';
import { CountUpNumber } from './count-up-number';

export const BasicCountUpNumber = () => (
  <CountUpNumber text="hello from CountUpNumber" />
);
