import React from 'react';

export type CountUpNumberProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function CountUpNumber({ text }: CountUpNumberProps) {
  return (
    <div>
      {text}
    </div>
  );
}
