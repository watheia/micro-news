import React from 'react';
import { render } from '@testing-library/react';
import { BasicCountUpNumber } from './count-up-number.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicCountUpNumber />);
  const rendered = getByText('hello from CountUpNumber');
  expect(rendered).toBeTruthy();
});
