import React from 'react';

export type SectionProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function Section({ text }: SectionProps) {
  return (
    <div>
      {text}
    </div>
  );
}
