import React from 'react';
import { render } from '@testing-library/react';
import { BasicSection } from './section.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicSection />);
  const rendered = getByText('hello from Section');
  expect(rendered).toBeTruthy();
});
