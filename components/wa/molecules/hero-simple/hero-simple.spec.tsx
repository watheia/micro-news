import React from 'react';
import { render } from '@testing-library/react';
import { BasicHeroSimple } from './hero-simple.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicHeroSimple />);
  const rendered = getByText('hello from HeroSimple');
  expect(rendered).toBeTruthy();
});
