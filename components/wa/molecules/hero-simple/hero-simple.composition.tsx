import React from 'react';
import { HeroSimple } from './hero-simple';

export const BasicHeroSimple = () => (
  <HeroSimple text="hello from HeroSimple" />
);
