import React from 'react';

export type HeroSimpleProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function HeroSimple({ text }: HeroSimpleProps) {
  return (
    <div>
      {text}
    </div>
  );
}
