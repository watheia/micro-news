import React from 'react';

export type DescriptionListProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function DescriptionList({ text }: DescriptionListProps) {
  return (
    <div>
      {text}
    </div>
  );
}
