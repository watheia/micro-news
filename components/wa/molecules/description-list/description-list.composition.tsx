import React from 'react';
import { DescriptionList } from './description-list';

export const BasicDescriptionList = () => (
  <DescriptionList text="hello from DescriptionList" />
);
