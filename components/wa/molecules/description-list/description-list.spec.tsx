import React from 'react';
import { render } from '@testing-library/react';
import { BasicDescriptionList } from './description-list.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicDescriptionList />);
  const rendered = getByText('hello from DescriptionList');
  expect(rendered).toBeTruthy();
});
