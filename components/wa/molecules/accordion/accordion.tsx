import React from 'react';

export type AccordionProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function Accordion({ text }: AccordionProps) {
  return (
    <div>
      {text}
    </div>
  );
}
