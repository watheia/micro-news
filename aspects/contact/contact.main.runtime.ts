import { MainRuntime } from '@teambit/cli';
import { ContactAspect } from './contact.aspect';

export class ContactMain {
  static slots = [];
  static dependencies = [];
  static runtime = MainRuntime;
  static async provider() {
    return new ContactMain();
  }
}

ContactAspect.addRuntime(ContactMain);
