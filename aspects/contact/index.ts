import { ContactAspect } from './contact.aspect';

export type { ContactMain } from './contact.main.runtime';
export default ContactAspect;
export { ContactAspect };
