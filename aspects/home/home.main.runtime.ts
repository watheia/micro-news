import { MainRuntime } from '@teambit/cli';
import { HomeAspect } from './home.aspect';

export class HomeMain {
  static slots = [];
  static dependencies = [];
  static runtime = MainRuntime;
  static async provider() {
    return new HomeMain();
  }
}

HomeAspect.addRuntime(HomeMain);
