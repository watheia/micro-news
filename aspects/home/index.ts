import { HomeAspect } from './home.aspect';

export type { HomeMain } from './home.main.runtime';
export default HomeAspect;
export { HomeAspect };
