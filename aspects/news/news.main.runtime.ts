import { MainRuntime } from '@teambit/cli';
import { NewsAspect } from './news.aspect';

export class NewsMain {
  static slots = [];
  static dependencies = [];
  static runtime = MainRuntime;
  static async provider() {
    return new NewsMain();
  }
}

NewsAspect.addRuntime(NewsMain);
