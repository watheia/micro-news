import { NewsAspect } from './news.aspect';

export type { NewsMain } from './news.main.runtime';
export default NewsAspect;
export { NewsAspect };
