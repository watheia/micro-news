/** @format */

import { ApolloProvider } from "@apollo/client"
import { useApollo } from "../apollo/client"
import Head from "next/head"

export default function App({ Component, pageProps }) {
  const apolloClient = useApollo(pageProps.initialApolloState)

  return (
    <>
      <Head>
        <title>Micro News by Watheia Labs</title>
      </Head>
      <ApolloProvider client={apolloClient}>
        <Component {...pageProps} />
      </ApolloProvider>
    </>
  )
}
