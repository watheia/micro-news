/** @format */

import { useEffect } from "react"
import { useRouter } from "next/router"
import Link from "next/link"
import { gql, useQuery } from "@apollo/client"

const PrincipalQuery = gql`
  query PrincipalQuery {
    principal {
      id
      email
    }
  }
`

const Index = () => {
  const router = useRouter()
  const { data, loading, error } = useQuery(PrincipalQuery)
  const principal = data?.principal
  const shouldRedirect = !(loading || error || principal)

  useEffect(() => {
    if (shouldRedirect) {
      router.push("/signin")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [shouldRedirect])

  if (error) {
    return <p>{error.message}</p>
  }

  if (principal) {
    return (
      <div>
        You're signed in as {principal.email} goto{" "}
        <Link href="/about">
          <a>about</a>
        </Link>{" "}
        page. or{" "}
        <Link href="/signout">
          <a>signout</a>
        </Link>
      </div>
    )
  }

  return <p>Loading...</p>
}

export default Index
