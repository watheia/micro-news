/** @format */

import { useState } from "react"
import styles from "../styles.module.css"

export default function Index() {
  const [response, setResponse] = useState()

  const makeRequest = async () => {
    const res = await fetch("/api/user")
    console.error("TODO: Not implemented", res)
    setResponse(response)
  }

  const { body, status, limit, remaining } = response
    ? response
    : { status: "", body: "", limit: 10, remaining: 10 }
  return (
    <main className={styles.container}>
      <h1>Next.js API Routes Rate Limiting</h1>
      <p>
        This example uses <code className={styles.inlineCode}>lru-cache</code> to implement
        a simple rate limiter for API routes (Serverless Functions).
      </p>
      <button onClick={() => makeRequest()}>Make Request</button>
      {response && (
        <code className={styles.code}>
          <div>
            <b>Status Code: </b>
            {status || "None"}
          </div>
          <div>
            <b>Request Limit: </b>
            {limit || "None"}
          </div>
          <div>
            <b>Remaining Requests: </b>
            {remaining || "None"}
          </div>
          <div>
            <b>Body: </b>
            {JSON.stringify(body) || "None"}
          </div>
        </code>
      )}
      <div className={styles.links}>
        <a href="#">View Source</a>
        {" | "}
        <a href="#">Deploy You Own ▲</a>
      </div>
    </main>
  )
}
