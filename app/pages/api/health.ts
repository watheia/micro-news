/** @format */

/**
 * Return and close immediately
 */
export default async function handler(_req, res) {
  res.setHeader("Connection", "close")
  res.status(200).json({ status: "UP" })
}
