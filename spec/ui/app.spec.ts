/** @format */

import "jasmine"

import { actorCalled } from "@serenity-js/core"
import { BrowseTheWeb } from "@serenity-js/protractor"
import { CallAnApi } from "@serenity-js/rest"
import { protractor } from "protractor"
import { EnsureTheServerIsUp } from "../api/screenplay"

describe(`app`, () => {
  it(`Serve the MicroNews site`, () =>
    actorCalled("Jasmine")
      .whoCan(BrowseTheWeb.using(protractor.browser), CallAnApi.at("http://localhost:8080"))
      .attemptsTo(EnsureTheServerIsUp()))
})
