/** @format */

import "jasmine"

import { actorCalled } from "@serenity-js/core"
import { CallAnApi } from "@serenity-js/rest"
import { EnsureTheServerIsUp } from "./screenplay"

describe(`API`, () => {
  it(`dev server startup`, () =>
    actorCalled("Bob")
      .whoCan(CallAnApi.at("http://localhost:8080"))
      .attemptsTo(EnsureTheServerIsUp()))
})
